Wander
======

A magic wand tool for removing colors from a raster image. Runs in your favorite modern browser that supports ES6.

The app's current color theme is Tomato Bisque.

### TODO:
- [x] pick a pixel
- [x] show output
- [x] download result
- [x] show edit info
- [x] better download name for non-test files
- [x] edit the edit
- [x] "drop file here - input"
- [ ] labels
