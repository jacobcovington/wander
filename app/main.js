import ImgEditor from "./editor";

const test_img_src = "/img/cartoon-hippo.gif";

const img_regex = /image\/(png|jpe?g|gif)/;
const images = [];

const upload_button = document.getElementById('FileUpload');
let main_app = undefined;

upload_button.addEventListener("change", upload, false);
document.addEventListener("dragover", drag, false);
document.addEventListener("drop", drop, false);

// dev/test init
if (/test/.test(window.location.search)) {
  let test_img = new Image();
  test_img.addEventListener("load", _ => imgToFile(test_img, renderEditors));
  test_img.src = test_img_src;
}

// helpers
function upload(e) {
  e.stopPropagation();
  e.preventDefault();
  renderEditors(e.target.files);
}

function drag(e) {
  e.stopPropagation();
  e.preventDefault();
  e.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
}

function drop(e) {
  e.stopPropagation();
  e.preventDefault();
  renderEditors(e.dataTransfer.files);
}

function renderEditors(at_least_one_file) {
  let files = ensureArray(at_least_one_file);
  let ul = document.createElement("ul");

  for (let i = 0, f; f = files[i]; i++) {
    if (img_regex.test(f.type)) {
      let editor = new ImgEditor(f);
      images.push(editor);
      ul.appendChild(editor.render());
    }
  }

  if (!main_app) {
    main_app = document.createElement("div");
    main_app.id = "Main";
    document.body.insertBefore(main_app, document.body.children[0]);
  }

  main_app.appendChild(ul);
}

function imgToFile(img, fun = noop) {
  createImageCanvas(img)
    .toBlob(fun);
}

function createImageCanvas(img) {
  let canvas = document.createElement("canvas");
  let ctx = canvas.getContext("2d");
  canvas.width = img.width;
  canvas.height = img.height;

  ctx.drawImage(img,0,0);
  return canvas;
}

function ensureArray(atom_or_list) {
  if (atom_or_list.length) {
    atom_or_list = Array.from(atom_or_list);
  }
  return Array.isArray(atom_or_list) ? atom_or_list : [atom_or_list];
}

function noop() {
  return undefined;
}
