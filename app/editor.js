import Edit from "./edit";
import * as PicUtils from "./pic_utils";

export default class ImgEditor {
  constructor(file) {
    this.file = file;
    this.edits = [];
    this.ctx = undefined;
    this.img_data = undefined;
  }

  render() {
    this.renderTitle();
    this.renderDisplay();
    // this.renderSource();
    this.renderEditList();

    return this.editor;
  }

  get width() {
    return this.ctx.canvas.width || 0;
  }

  get height() {
    return this.ctx.canvas.height || 0;
  }

  get html_details() {
    return `<strong>${this.name}</strong> (${this.file.type}) - ${this.file.size} bytes`;
  }

  get name() {
    return this.file.name ? escapeHTML(this.file.name) : "this is a blob";
  }

  get downloadName() {
    if (this.file.name) {
      return this.file.name.replace(/\.[0-9a-z]+$/i, '') + "_edit";
    } else {
      return "test-blob-edit";
    }
  }

  onDisplayClick(e) {
    let edit = Edit.fromEvent(e);

    edit.draw(this.ctx);

    this.edits.push(edit);
    this.edit_list.appendChild(edit.toRow());
  }

  onEditChange(e) {
    let changed = e.target;
    if (changed.classList.contains("tolerance")) {
      let row = changed.closest("li.edit");
      let i = Array.from(e.currentTarget.children).indexOf(row);

      if (changed.value !== this.edits[i].tolerance) {
        Array.from(row.querySelectorAll(".tolerance"))
          .forEach(t => t.value = changed.value);

        window.requestAnimationFrame(() => {
          this.edits[i].tolerance = +changed.value;
          this.ctx.clearRect(0, 0, this.width, this.height);
          PicUtils.drawFromData(this.img_data, this.ctx)
            .then(this.processEdits.bind(this));
        });
      }
    }
  }

  processEdits() {
    this.edits.forEach(e => e.draw(this.ctx));
  }

  download(e) {
    e.target.href = this.ctx.canvas.toDataURL('image/png');
  }

  renderTitle() {
    this.editor = document.createElement("li");
    this.editor.className = "img-editor";

    let head = document.createElement('h3');
    head.className = "editor-head";
    head.innerHTML = this.html_details + " - ";
    head.appendChild(this.downloadButton());

    this.editor.appendChild(head);
  }

  downloadButton() {
    let download = document.createElement("a");
    download.setAttribute("download", `${this.downloadName}.png`);
    download.className = "download-link";
    download.innerHTML = "download";

    download.addEventListener("click", this.download.bind(this));

    return download;
  }

  renderDisplay() {
    let display = document.createElement("canvas");
    display.className = "display";
    display.addEventListener("click", this.onDisplayClick.bind(this));

    this.ctx = display.getContext("2d");
    this.editor.appendChild(display);

    PicUtils.asDataURL(this.file).then(img_data => {
      this.img_data = img_data;
      PicUtils.drawFromData(img_data, this.ctx);
    });
  }

  renderSource() {
    let source = document.createElement("div");
    source.className = "source";
    source.title = this.name;
    this.editor.appendChild(source);

    this.asDataURL(img_data => {
      source.style.backgroundImage = `url(${img_data})`;
    });
  }

  renderEditList() {
    let edit_list = document.createElement("ul");
    edit_list.className = "edits";
    edit_list.addEventListener("change", this.onEditChange.bind(this));
    this.editor.appendChild(edit_list);

    this.edit_list = edit_list;
  }
}

let sanitized_html = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#039;'
};

function escapeHTML(text) {
  return text.replace(/[&<>"']/g, m => sanitized_html[m]);
}
