const MAX_TOLERANCE = 442;

export default class Edit {
  x: number
  y: number
  tolerance: number
  rgba: [number]

  constructor(x, y, color) {
    this.x = x;
    this.y = y;
    this.tolerance = 10;

    this.rgba = color;
  }

  get hex() {
    return Array.from(this.rgba).slice(0, 3).map(c => c.toString(16).toLocaleUpperCase()).join("");
  }

  static fromEvent(e) {
    let x_scale = e.target.width / e.target.offsetWidth;
    let y_scale = e.target.height / e.target.offsetHeight;
    let el_pos = elementOffset(e.target);
    let x = (e.pageX - el_pos.x) * x_scale | 0;
    let y = (e.pageY - el_pos.y) * y_scale | 0;

    let ctx = e.target.getContext("2d");
    let color = ctx.getImageData(x, y, 1, 1).data;

    return new Edit(x, y, color);
  }

  toRow() {
    let el = document.createElement("li");
    let rgb = this.rgba.slice(0, 3).join(", ");
    let rgb_span = `<span class="rgb">(${rgb})</span>`;
    let hex_span = `<span class="hex">#${this.hex}</span>`;
    let tolerance = `
<input value="${this.tolerance}"
       class="tolerance number"
       min="0"
       max="${MAX_TOLERANCE}"
       type="number"
/>
<input value="${this.tolerance}"
       class="tolerance range"
       min="0"
       max="${MAX_TOLERANCE}"
       type="range"
/>`

    el.innerHTML = `
<span class="color" style="background-color:rgb(${rgb})"></span>
<p>${hex_span} ${rgb_span}</p>
<p>${tolerance}</p>`;

    el.className = "edit";
    return el;
  }

  // useful for debugging
  drawPoint(ctx) {
    ctx.fillStyle = "red";
    ctx.fillRect(this.x - 2, this.y - 2, 4, 4);
    console.log(this.x, this.y)
  }

  draw(ctx) {
    let begin = Date.now();

    let scale = 4;
    let width = ctx.canvas.width;
    let height = ctx.canvas.height;
    let img_size = width * height;
    let visited = new Array(img_size);

    let img_data = ctx.getImageData(0, 0, width, height);
    let pixels = img_data.data;

    let start = this.x + (this.y * width);
    let scaled_start = start * scale;

    let rgb = pixels.slice(scaled_start, scaled_start+4);

    let to_visit = [start];
    let i = 0;
    let visit = idx => {
      visited[idx] = true;
      to_visit.push(idx);
    };
    let shouldVisit = idx => {
      if (visited[idx]) return false;

      let s = idx * 4,
          r = rgb[0] - pixels[s],
          g = rgb[1] - pixels[s+1],
          b = rgb[2] - pixels[s+2],
          distance = Math.sqrt((r * r) + (g * g) + (b * b));

      return distance <= this.tolerance;
    };

    while (i < to_visit.length) {
      let middle = to_visit[i];
      let mid_x = middle % width;

      // make current pixel transparent
      pixels[(middle*4)+3] = 0;

      let up = middle - width;
      if (up >= 0 && shouldVisit(up)) {
        visit(up);
      }

      let right = middle + 1;
      if (right % width > mid_x && shouldVisit(right)) {
        visit(right);
      }

      let down = middle + width;
      if (down < img_size && shouldVisit(down)) {
        visit(down);
      }

      let left = middle - 1;
      if (left % width < mid_x && shouldVisit(left)) {
        visit(left);
      }

      i++;
    }

    // let time = Date.now() - begin;
    // console.log('checked', to_visit.length, edit,
    //             (time/1000)+"s",
    //             to_visit.length/time);

    ctx.putImageData(img_data, 0, 0);
  }
}

function elementOffset(el) {
  let curleft = 0, curtop = 0;
  if (el.offsetParent) {
    do {
      curleft += el.offsetLeft;
      curtop += el.offsetTop;
    } while ((el = el.offsetParent));
  }

  return { x: curleft, y: curtop };
}
