export function drawFromData(img_data, ctx) {
  return new Promise((resolve, reject) => {
    let img = new Image();
    img.onload = _ => {
      ctx.canvas.width = img.naturalWidth || img.width;
      ctx.canvas.height = img.naturalHeight || img.height;

      ctx.drawImage(img, 0, 0);

      resolve(ctx);
    };
    img.src = img_data;
  });
}

export function asDataURL(file) {
  return new Promise((resolve, reject) => {
    let reader = new FileReader();
    reader.onload = e => resolve(e.target.result);
    reader.readAsDataURL(file);
  });
}

export function clear(ctx) {
  ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
}
